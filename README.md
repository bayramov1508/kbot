### Telegram_bot Project Description 

#### Frameworks:
[Cobra](https://github.com/spf13/cobra) and [Telebot.v3](https://gopkg.in/telebot.v3)

### Features
- [x] Implement message handlers for the bot that will respond to Telegram messages;
- [x] Create bot message handler functions;
- [x] Add these functions to the methods of the telebot.Bot object;
- [x] Process messages according to their type and content.
- [x] Integrate Makefile and Dockerfile for automation


### :tada: Results :tada:

t.me/ibayro_bot - this is a trivial Telegram bot with minimal functional.

New features are to be implemented (TBA)


